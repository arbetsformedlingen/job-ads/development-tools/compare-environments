# -*- coding: utf-8 -*-
import os
from distutils.util import strtobool

CERTIFICATE_VERIFY = strtobool(os.getenv("CERTIFICATE_VERIFY", "true"))


# Runs a subset of the tests. Set the number of tests to run from each test case file
# if value is 0 (zero) all test cases are executed
SMOKE = int(os.getenv("SMOKE", 0))


# allow difference of THRESHOLD more or less when comparing environments to reduce "noise"
THRESHOLD = int(os.getenv("THRESHOLD", 5))

# allow percentage difference of THRESHOLD_PERCENT more or less (e.g. 5 == 95% - 105%)  when comparing environments to reduce "noise"
THRESHOLD_PERCENT = int(os.getenv("THRESHOLD_PERCENT", 5))

# log level for Loguru logger
LOGURU_LEVEL = os.getenv("LOGURU_LEVEL", "INFO")

# true if metadata about the environments should be written to file, false otherwise
WRITE_METADATA_HEADERS = strtobool(os.getenv("WRITE_METADATA_HEADERS", "false"))

# Environments
# usually production environment, or main branch if running locally
REFERENCE_URL = os.getenv("REFERENCE_URL", 'http://127.0.0.1:5000/search')

# Url to the new code that you want to compare against the reference environment
TARGET_URL = os.getenv("TARGET_URL", 'http://127.0.0.1:5100/search')

# "jobsearch" or "joblinks". This is used for file name matching to get the right test cases
API_TYPE = os.getenv("API_TYPE", "jobsearch")
