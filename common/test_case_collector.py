# -*- coding: utf-8 -*-
import glob
import pathlib

from loguru import logger
import settings
from common.constants import JOBLINKS, JOBSEARCH, JOBSTREAM


def get_test_case_files() -> list:
    api_type = settings.API_TYPE.lower()
    if api_type not in [JOBLINKS, JOBSEARCH, JOBSTREAM]:
        logger.error(f"Unknown api type: {api_type}")
        test_case_files = []
    else:
        logger.info(f"Collection test cases for {api_type}")
        test_case_dir = pathlib.Path(__file__).parent.parent.resolve() / "test_cases"
        test_case_files = glob.glob(f"{test_case_dir}\\*{settings.API_TYPE}*.txt")
        logger.debug(test_case_files)

    return test_case_files
