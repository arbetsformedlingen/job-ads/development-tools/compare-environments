import datetime
from common import constants


def date_converter(date_modifier: str) -> str:
    now = datetime.datetime.now()
    formatted_date = (now - datetime.timedelta(days=int(date_modifier))).strftime(constants.DATE_FORMAT)
    return formatted_date


def convert_test_case_date(test_case: str) -> str:
    """
    Parses a test case string and replaces the date placeholder with a new date relative to current date
    :param test_case: string with date placeholder in the format DATE-* which means * days before current day
    :return: string with placeholder replaced by real date (today - modifier days)
    """
    test_case = test_case.strip()
    for place_holder in [constants.DATE_PLACEHOLDER, constants.UPDATED_BEFORE_DATE_PLACEHOLDER]:
        if place_holder in test_case:
            position = (test_case.find(place_holder) + len(place_holder))  # start at end of placeholder substring
            substring_after_placeholder = test_case[position:]
            date_modifier = ""
            for char in substring_after_placeholder:
                if char != "&":
                    date_modifier += char
                else:
                    break

            sub_string_to_replace = place_holder + date_modifier
            dynamic_date = f"date={date_converter(date_modifier)}".replace(":", "%3A")
            test_case = test_case.replace(sub_string_to_replace, dynamic_date)

    return test_case
