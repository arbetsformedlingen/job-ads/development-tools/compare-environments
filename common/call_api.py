import json

import requests

from loguru import logger

import settings
from common import constants


def get_total(url: str, query_string: str) -> int:
    url_qs = f"{url}?{query_string}"
    logger.trace(url_qs)
    try:
        response = requests.get(url_qs, headers={'accept': 'application/json'}, verify=settings.CERTIFICATE_VERIFY)
        response.raise_for_status()
    except Exception as e:
        logger.error(e)
        return 9999999

    decoded_content = json.loads(response.content.decode('utf8'))
    if settings.API_TYPE == constants.JOBSTREAM:
        return len(decoded_content)
    else:
        return int(decoded_content["total"]["value"])
