import pytest
from common.helper import is_diff_percent, is_diff_absolute


@pytest.mark.parametrize("ref, target", [
    (1000, 970),
    (2277, 2275),
    (2277, 2276),
    (22200, 22001),
    (22200, 22000),
    (1000, 960),
])
def test_percent_not_diff(ref, target):
    result, diff_pct = is_diff_percent(ref, target)
    assert not result, diff_pct


@pytest.mark.parametrize("ref, target", [
    (34, 55),
    (22200, 21000),
    (22200, 18000),
    (22200, 0),
    (100, 95),
])
def test_percent_diff(ref, target):
    result, diff_pct = is_diff_percent(ref, target)
    assert result, diff_pct


@pytest.mark.parametrize("ref, target", [
    (34, 55),
    (22200, 21000),
    (22200, 18000),
    (22200, 0),
])
def test_is_diff_absolute(ref, target):
    assert is_diff_absolute(ref, target)


@pytest.mark.parametrize("ref, target", [
    (34, 35),
    (34, 36),
    (34, 39),
    (2200, 2204),
    (2200, 2205),
])
def test_is_diff_absolute(ref, target):
    assert not is_diff_absolute(ref, target)
