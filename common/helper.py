# -*- coding: utf-8 -*-
import ntpath
import os
from urllib.parse import unquote

from loguru import logger

import settings


def elapsed_time(seconds) -> str:
    hours, remainder = divmod(seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{minutes} minutes, {seconds} seconds"


def is_relevant_diff(ref_value: int, target_value: int, query_string: str) -> (bool, float):
    is_diff_pct, percent = is_diff_percent(ref_value, target_value)
    diff_absolute = is_diff_absolute(ref_value, target_value)

    if (ref_value > 0 and target_value == 0) and (diff_absolute > settings.THRESHOLD):
        msg = f"Target has zero hits. Ref: {ref_value} target: {target_value}. {query_string} "
        logger.error(msg)
        zero_hits = True
    else:
        zero_hits = False
    # relevant diff:
    # - to large diff in percent and absolute or zero hits in target when ref has hits
    return ((is_diff_pct and diff_absolute) or zero_hits), percent


def is_diff_absolute(ref_value: int, target_value: int) -> bool:
    diff = target_value - ref_value
    return diff > settings.THRESHOLD or diff < -settings.THRESHOLD


def is_diff_percent(ref_value: int, target_value: int) -> (bool, float):

    if ref_value == 0 and target_value == 0:
        result = False
        diff_percent = 0
    else:
        if ref_value == 0:
            # avoid zero division error
            ref_value = 0.0000000001

        diff = target_value - ref_value
        diff_percent = (diff / ref_value) * 100

        acceptable_percent_diff = settings.THRESHOLD_PERCENT
        lower_percent_limit = float(1 - acceptable_percent_diff)
        upper_percent_limit = float(1 + acceptable_percent_diff )
        result = diff_percent != 0 and (diff_percent < lower_percent_limit or diff_percent > upper_percent_limit)
    return result, round(diff_percent, 3)


def result_formatter(query_string: str, ref_value: int, target_value: int, percent: float) -> str:
    diff = target_value - ref_value
    qs = unquote(query_string)
    if len(qs) > 20:
        qs = f"{qs[:20]}..."
    pct = int(percent * 100)
    if pct == 0:
        x = None
    result = f"{qs} |  {ref_value} | {target_value} | {diff} | {pct} | {query_string}"
    return result


def write_header(file_name, total_ref, total_target, smoke_text) -> None:
    file_dir_path = ntpath.dirname(file_name)

    if not os.path.isdir(file_dir_path):
        os.mkdir(file_dir_path)

    with open(file_name, "w", encoding="utf8") as result_file:
        if settings.WRITE_METADATA_HEADERS:
            result_file.write(f"Compare  | Reference | Target | | |\n")
            result_file.write(f"URL:s  | {settings.REFERENCE_URL} | {settings.TARGET_URL} | | |\n")
            result_file.write(f"Threshold | {settings.THRESHOLD} |  | |\n")
            result_file.write(f"Smoke | {smoke_text} |  | | |\n")
            result_file.write(f"Total number of ads  | {total_ref} | {total_target} |  | |\n")

        result_file.write(f"Test case  | Reference | Target | Diff |Diff % | Full test case\n")
