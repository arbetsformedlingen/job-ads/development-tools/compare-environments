# -*- coding: utf-8 -*-
import datetime

from loguru import logger

import settings
from common import constants
from common import test_case_collector
from common.call_api import get_total
from common.helper import elapsed_time, is_relevant_diff, result_formatter, write_header
from common.date_handler import convert_test_case_date

REF_URL = settings.REFERENCE_URL
TARGET_URL = settings.TARGET_URL


def run_test_cases():
    logger.info(f"start of comparison of {settings.API_TYPE}")
    test_case_files = test_case_collector.get_test_case_files()
    start = datetime.datetime.now()
    start_time = start.strftime("%Y-%m-%d-%H-%M-%S")

    file_base = f"output\\{settings.API_TYPE}-results-{start_time}"
    diff_results_file = f"{file_base}_diff.csv"
    logger.info(f"Threshold for showing differences: {settings.THRESHOLD}")
    smoke = "Yes" if settings.SMOKE > 0 else "No"
    number_of_smoke_tests = f"{settings.SMOKE}" if settings.SMOKE > 0 else "all"
    logger.info(f"Smoke test? {smoke}, running {number_of_smoke_tests} tests from each file")
    logger.info(f"Comparing {TARGET_URL} to reference environment {REF_URL}")

    if settings.API_TYPE != constants.JOBSTREAM:
        total_ref = get_total(REF_URL, "limit=0")
        total_target = get_total(TARGET_URL, "limit=0")
        write_header(diff_results_file, total_ref, total_target, smoke)

    with open(diff_results_file, "a", encoding="utf8") as result_file:
        total_test_cases = 0
        for file in test_case_files:
            logger.info(f"Running test cases from {file}")
            with open(file, encoding="utf8") as f:
                counter = 0

                for line in f:
                    if counter >= settings.SMOKE and settings.SMOKE > 0:
                        logger.info(f"Smoke test, stopped processing {file}")
                        break
                    counter += 1
                    total_test_cases += 1
                    tc = convert_test_case_date(line)

                    ref_value = get_total(REF_URL, tc)
                    target_value = get_total(TARGET_URL, tc)
                    diff_result, percent = is_relevant_diff(ref_value, target_value, tc)
                    if diff_result:
                        result = result_formatter(tc, ref_value, target_value, percent)
                        result_file.write(f"{result}\n")
                        if int(percent) < 1:
                            logger.warning(f"Diff is lower than 1% - {result}")

                logger.info(f"Ran {counter} test cases from {file}")

    logger.info(f"Completed {total_test_cases} test cases in {elapsed_time((datetime.datetime.now() - start).seconds)}")
    logger.info(f"Diff results file saved at {diff_results_file}")


if __name__ == '__main__':
    run_test_cases()
