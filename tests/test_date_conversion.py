import pytest

from common import date_handler

"""
Tests do not really test because dates are dynamic, but results are printed 
"""


@pytest.mark.parametrize("test_case", [
    "DATE-1",
    "DATE-1&location-concept-id=bUNp_6HW_Vbn",
    "DATE-10&location-concept-id=bUNp_6HW_Vbn",
    "DATE-100&location-concept-id=bUNp_6HW_Vbn",
])
def test_date_converter(test_case):
    converted_test_case = date_handler.convert_test_case_date(test_case)
    print(f"Test case: {test_case}, after conversion: {converted_test_case}")


@pytest.mark.parametrize("date_modifier", ["10", "1", "111", "0"])
def test_date_conversion(date_modifier):
    formatted_date = date_handler.date_converter(date_modifier)
    print(f"Modifier: {date_modifier}, new date {formatted_date}")
